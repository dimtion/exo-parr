#!/usr/bin/env bash

set +o pipefail

xargs -I% -L1 wget -q -O - "%" | grep -IE -o 'href="https?://[^"]*"' | sed -r 's/href="(.*)"/\1/'

