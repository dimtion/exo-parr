OUTPUT=rapport

all: $(OUTPUT).html $(OUTPUT).pdf

$(OUTPUT).html: $(OUTPUT).md Makefile
	pandoc $(OUTPUT).md --metadata pagetitle="Rapport parallélisation" -s -o $(OUTPUT).html

$(OUTPUT).pdf: $(OUTPUT).md Makefile
	pandoc $(OUTPUT).md -s -o $(OUTPUT).pdf
