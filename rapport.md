Rapport parallélisation
========================

Pour : adrien.guinet@telecom-bretagne.eu

## Extraction de pages

Informations sur la machine d'expérience :

- Système d'exploitation : Ubuntu 18.04.1 LTS, Linux 4.15.0-43-generic
- Processeur : Intel(R) Core(TM) i3-4150 CPU @ 3.50GHz
- Réseau : Ethernet 1000BaseT, sur la DISI IMT-Atlantique (via RENATER)
- Outils : wget 1.19.4


### Étude initiale

Nous sommes chargé d'étudier un simple script shell qui prendre en entrée des
URLs, télécharger ces pages avec `wget` et à l'aide de `sed`, `grep` et de
plusieurs regex extraire de chaque page les adresses URL présentes.

Dans la version initiale de l'outil nous avons déjà une forme de parallélisme
du fait que l'extraction des URL se fait en même temps que le téléchargement
des pages. Cependant le téléchargement de plusieurs pages se fait
séquentiellement.

On peut vérifier cela avec un petit script `bench.sh` :
```bash
#!/usr/bin/env bash

bash -c "echo resel.fr | ./page.sh > /dev/null" &

ps f -o pid,stat,cmd -g $(ps -o sid= -p $!) 
wait $(jobs -p)
```

On constate bien wget, grep et sed sont lancés en même temps :
```
$ ./bench.sh 
  PID STAT CMD
18252 Ss   -bash
18562 S+    \_ bash ./bench.sh
18563 S+        \_ bash -c echo resel.fr | ./page.sh > /dev/null
18566 S+        |   \_ bash ./page.sh
18567 S+        |       \_ bash ./page.sh
18570 S+        |       |   \_ wget -O - resel.fr
18568 S+        |       \_ grep -E -o href="https?://[^"]*"
18569 S+        |       \_ sed -r s/href="(.*)"/\1/
18571 R+        \_ ps f -o pid,stat,cmd -g 18252
```

Si on change le script pour faire passer la sortie du scrapping sur lui même on
peut crééer un crawler récursif (à un seul niveau pour le moment). On constate
que tous les niveaux sont exécutés en parallèle :

```bash
#!/usr/bin/env bash

bash -c "echo redhat.com | ./page.sh | ./page.sh > /dev/null" &

ps f -o pid,stat,cmd -g $(ps -o sid= -p $!) 
wait $(jobs -p)
```

On voit bien le parallélisme minimal fait : le crawl du deuxième niveau est
fait dès lors que le premier niveau renvoit une nouvelle ligne :

```
$ ./bench.sh 
  PID STAT CMD
18252 Ss   -bash
19462 S+    \_ bash ./bench.sh
19463 S+        \_ bash -c echo redhat.com | ./page.sh | ./page.sh > /dev/null
19466 S+        |   \_ bash ./page.sh
19468 S+        |   |   \_ bash ./page.sh
19474 S+        |   |   |   \_ wget -O - redhat.com
19470 S+        |   |   \_ grep -E -o href="https?://[^"]*"
19473 S+        |   |   \_ sed -r s/href="(.*)"/\1/
19467 S+        |   \_ bash ./page.sh
19469 S+        |       \_ bash ./page.sh
19471 S+        |       \_ grep -E -o href="https?://[^"]*"
19472 S+        |       \_ sed -r s/href="(.*)"/\1/
19475 R+        \_ ps f -o pid,stat,cmd -g 18252
```

### Analyse des performances initiales

Les performances de ce crawler dépend fortement du temps de réponse du serveur
distant. Si on crawl un site standard dont la page ne fait que quelques
méga-octets le temps de parsing est faible devant le temps d'interrogation du
serveur et le temps de téléchargement.

Afin d'analyser les performances nous allons fixer un site à crawler
(http://www.redhat.com) et supposer que celui-ci ne changera que peu durant le
temps de l'expérience. De plus nous supposerons que les latences d'accès soient
relativement constants. Cette seconde suppostion étant assez grotesque nous
allons faire plusieurs mesures par expérience et moyenner les résultats.

Récursivité = 1
Itérations = 10
```
$ DATE="$(date --iso-8601=s)"
$ SITE="www.redhat.com"
$ ITER=10
$ time bash -c "for i in $(seq %ITER); do echo $SITE | ./page.sh > "urls-for-$SITE-$DATE.txt" ; done"

real    0m1,751s
user    0m0,173s
sys     0m0,083s
```

Les résultats confirment bien les suppositions : le temps système (dans le
Kernel) et le temps utilisateur (dans le userland) sont très faible, tandis que
le temps total est élevé (au moins un facteur 10). Cela confirme que le
processus passe beaucoup de temps en I/O, c'est à dire en attente du réseau.
(cf [réponse SO complète](https://stackoverflow.com/a/556411) et la page man de
time(1)). 

Nous constatons le même résultat pour des niveaux de récursivité supérieurs :
le temps total est beaucoup plus grand que les temps user et sys. Il est donc
pertinent pour optimiser le crawling dans un premier temps que d'optimiser la
variable `real time`.

Le tablau suivant présente le temps mesuré pour d'autres niveaux d'itération,
cela sera utile pour mesurer l'accélération par la suite. Voici les temps
_réels_ mesurés :

| Recursivité | Nombre de mesures | temps total (s) | temps moyen (s) |
| ---         | ---               | ---             | ---             |
| 1           | 10                | 1.76            | 0.176           |
| 2           | 10                | 180.00          | 18.00           |
| 3           | 10                | 432.20          | 43.22           |


### Première optimisation : utilisation naïve de background jobs

Pour commencer nous pouvons lancer les processus `wget` en parallèle afin de
faire le téléchargement des pages du même niveau de récursivité en même temps.
Le nouveau code de `page.ressemble à ceci :

```bash
#!/usr/bin/env bash

set +o pipefail

while read line
do
    wget -q -O - "$line" || true &
done | grep -IE -o 'href="https?://[^"]*"' | sed -r 's/href="(.*)"/\1/'
wait $(jobs -p)
```

En effectuant les même benchmark que précédement nous obtenons les temps
suivants :

| Recursivité | Nombre de mesures | temps total (s) | temps moyen (s) | accélération moyenne |
| ---         | ---               | ---             | ---             | ---                  |
| 1           | 10                | 1.40            | 0.14            | 1.26                 |
| 2           | 10                | 33.70           | 3.37            | 5.34                 |
| 3           | 10                | 48.00           | 4.80            | 9.00                 |

Cette technique a cependant quelques limitations :
- Il n'est pas possible facilement de controller le nombre de processus wget
  lancés, celui-ci peut augmenter très rapidement avec le nombre de liens
  visités
- Nous pouvons arriver aux limites du système d'exploitation du nombre de
  processus lancés `sysctl kernel.pid_max` ou les limites utilisateur `ulimit
  -a`
- Avec un très grand nombre de processus lancés nous pouvons avoir des
  contentions réseaux en voulant ouvrir trop de connexions HTTP en parrallèle.

Sans surprise l'accélération augmente avec la récursivité car le nombre de
processus lancés augmente également, cependant nous n'avons pas réussi à
mesurer un épuissement d'un ressource ou une contention qui limiterait
l'accélération. Pour cela nous allons limiter le nombre de recusivité à 2 et
controller le nombre de processus lancés à l'aide de `xargs`.

### Deuxième optimisation : `xargs` à la rescousse

Cette proposition d'optimisation permet avec l'aide de xargs de controller le
nombre de processus lancés en parallèle. Le script `page.sh` devient alors :

```bash
#!/usr/bin/env bash

set +o pipefail
PARR=1

xargs -I% -L1 -P$PARR wget -q -O - "%" | grep -IE -o 'href="https?://[^"]*"' | sed -r 's/href="(.*)"/\1/'
```

Résultat du benchmark :
 - récursion : 2
 - site : https://redhat.com
 - mesures : 10
 - Temps de référence (implémentation initiale) : 18.0 s

| # processus | temps total (s) | temps moyen (s) | accélération moyenne |
| ---         | ---             | ---             | ---                  |
| 1           | 250.5           | 25.05           | 0.72                 |
| 2           | 133,41          | 13.34           | 1.35                 |
| 5           | 64.26           | 6.43            | 2.80                 |
| 10          | 44.60           | 4.46            | 4.00                 |
| 20          | 44.12           | 4.12            | 4.36                 |
| 50          | 41.83           | 4.18            | 4.30                 |
| 100         | 34.39           | 3.43            | 5.25                 |
| 200         | 34.97           | 3.97            | 4.53                 |
| 1000        | 28.45           | 2.28            | 7.89                 |
| 1000        | 28.45           | 2.28            | 7.89                 |
| 5000        | 31.64           | 3.16            | 5.69                 |

Nous constatons qu'au delà de 100 processus lancés l'accélération plateau.
Cela peut être expliqué par le fait qu'il n'y ait que 124 pages explorés
au total. Augmenter le nombre de processus de scrapping n'a donc pas d'intérêt.
Il est important de noter que le nombre de processus à choisir dépend du site à
scrapper. Dans notre cas une valeure de `100` processus semble convenir. Et
fournit une accélération moyenne de ~5.

L'avantage de limiter le nombre de processus lancés est que nous pouvons avoir
un controlle sur les ressources du scrapping comme le nombre de sockets ouverts
ce qui permet de ne pas saturer la machine ou l'utilisateur si la machine est
utilisée pour d'autres tâches.


## Min / Max

L'objectif de cet exercice est de calculer le minimum et le maximum d'un
tableau de flottants. Et d'optimiser cette implémentation. En théorie, seul le
code de la fonction `void minmax(...)` est à modifier.

Informations sur la machine d'expérience :

- Système d'exploitation : MacOS 10.14.1
- Processeur : Intel(R) Core(TM) i5-8259U CPU @ 2.30GHz 
- Instructions gérées : FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE
  MCA CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE SSE2 SS HTT TM PBE SSE3
  PCLMULQDQ DTES64 MON DSCPL VMX EST TM2 SSSE3 FMA CX16 TPR PDCM SSE4.1 SSE4.2
  x2APIC MOVBE POPCNT AES PCID XSAVE OSXSAVE SEGLIM64 TSCTMR **AVX1.0** RDRAND
  F16C
- compilateur: Apple LLVM version 10.0.0 (clang-1000.10.44.4) basé sur LLVM 6.0.1


## Implémentation naïve

Pour servir de référence, nous pouvons faire une première implémetation naïve de la fonction :
```c
static void minmax(float const* start, float const* stop, float* min, float* max) {
  *min = *start;
  *max = *start;
  for (float const* c = start; c < stop; c++) {
    if (*c < *min) {
      *min = *c;
    }
    if (*c > *max) {
      *max = *c;
    }
  }
}
```

Options de compilation : aucune optimisation pour le test  de référence, la
compilation est faite sans flags.

C'est une implémentation simple qui théoriquement s'execute en O(n) en temps et
en O(1) en espace. Nous pouvons vérifier simplement en faisant un test avec une
taille de tableau variable.

On peut rapidement vérifier cela:
```bash
#!/usr/bin/env bash

for i in 1000 5000 10000 50000 100000 500000 1000000 5000000 10000000 50000000 100000000 500000000 10000000000 ; do 
        echo "$i: $(./$1 $i | grep ms)";
done
```

Nous commencons à 100 itérations car en dessous le temps est tellement faible
qu'il n'est pas mesurable avec la métrique que nous utilisons.


![](images/min_max_naive_bench.png)


Nous constatons que le temps d'exécution est parfaitement linéaire, cela
correspond parfaitement à la théorie. Nous aurions pu imaginer que le temps
d'exécution pouvait avoir une variance plus forte de fait de caches diverses.

TODO: calculer la taille des caches CPU et de la RAM pour voir comment peut s'appliquer cette théorie

### Implémentation vectorielle

```c
#ifdef AVX
#define VEC_LEN 8
static void minmax(float const* start, float const* stop, float* min, float* max) {
        float max_table[VEC_LEN];
        float min_table[VEC_LEN];
        size_t i;

        __m256 _vec;
        __m256 _max;
        __m256 _min;

        *max = *start;
        *min = *start;

        if (stop - start <= VEC_LEN) {
                for (const float*  c = start; stop; c++) {
                        if (*c > *max) *max = *c;
                        if (*c < *min) *min = *c;
                }
                return;
        }

        _max = _mm256_loadu_ps(start);
        _min = _mm256_loadu_ps(start);
        for (i = 0; i < (stop - start) / VEC_LEN; i++) {
                _vec = _mm256_loadu_ps(start + i * VEC_LEN);
                _max = _mm256_max_ps(_max, _vec);
                _min = _mm256_min_ps(_min, _vec);
        }
        // do the last element bit not multiple of 8
        // twice if necessary, does not matters.
        _vec = _mm256_loadu_ps(stop - VEC_LEN);
        _max = _mm256_max_ps(_max, _vec);
        _min = _mm256_min_ps(_min, _vec);

        _mm256_store_ps(max_table, _max);
        _mm256_store_ps(min_table, _min);

        // consume the max_table/min_table into the final result
        for (i = 0; i < VEC_LEN; i++) {
                if (max_table[i] > *max) *max = max_table[i];
                if (min_table[i] < *min) *min = min_table[i];
        }
}
#endif
```

Nous avons réimplémenté la fonction `void minmax(...)` en utilisant les
instructions vectorielles AVX pour le calcul du maximum et du minimum. Cette implémentation est toujours en O(n) en temps et en O(1) en espace.

Nous pouvons constater l'accélération par rapport à l'implémentation initiale sur les graphes suivants :

![](images/avx_vs_naive.png)
![](images/avx_vs_naive_acc.png)

Nous constatons d'abord de façon évidente que l'accélération est toujours
supérieure à 1, ce qui montre que dans tous les cas d'usages mesurés
l'implémentation vectorielle est plus rapide. 

Comme nous utilisons des vecteurs de taille 8 (`VEC_LEN`) nous aurions pu
imaginer naivement que l'accélération serait constante à x8, ce qui n'est pas
le cas. Nous pouvons tout de même noter que :

- Sur l'intervalle $\left[ 10\mathrm{e}{5}, 10\mathrm{e}{8} \right]$
  l'accélération _est de l'ordre de 10_ ce qui correspond bien à une
  vectorialisation sur 256 bits (ou 8 flottants)
- Au delà de $10\mathrm{e}{8}$ l'accélération diminue et l'avantage de la
  vectorialisation décroit une étude de la stack à l'aide d'un flamegraphe
  pourrait aider à expliquer. TODO
- Pour $10\mathrm{e}{4}$ l'accélération est très grande


## Tri parallèle

L'objectif de cet exercice est créer une implémentation d'un algorithme de tri
parallèle à l'aide d'OpenMP et d'en comparer les performances avec un
algorithme de tri séquentiel (`std::sort`).

Informations sur la machine d'expérience :

- Système d'exploitation : Ubuntu 18.04.1 LTS, Linux 4.15.0-43-generic
- Processeur : Intel(R) Core(TM) i3-4150 CPU @ 3.50GHz, **2 coeurs 4 threads**
- Instructions gérées : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge
  mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx
  pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology
  nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2
  ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 movbe popcnt
  tsc_deadline_timer aes xsave **avx** f16c rdrand lahf_lm abm cpuid_fault epb
  invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid
  fsgsbase tsc_adjust bmi1 **avx2** smep bmi2 erms invpcid xsaveopt dtherm arat pln
  pts flush_l1d
- Compilateur : gcc 7.3.0


### Parallélisation du tri

La première optimisation que nous pouvons faire est de découper le tableau à
trier et de trier parallèlement chacun des sous-tableaux. Enfin à la fin un
merge est fait afin de réunir tous les tableaux :

```cpp
#define SORT_E double
#define PARR 4

void sort(SORT_E *data, int len) {
	int i;
        #pragma omp parallel for
	for(i=0; i < PARR; i++) {
		int l;
		if (i == PARR) {
			l = len;
		} else {
			l = (i + 1) * (len / PARR);
		}
		std::sort(
			data + i * (len / PARR),
			data + (i + 1) * (len / PARR)
		);
	}
	int depth = PARR;
	for (depth=PARR; depth > 1; depth>>=1) {
                #pragma omp parallel for
		for (i=0; i < depth; i+=2) {
			int l;
			if (i + 2 == depth) {
				l = len;
			} else {
				l = (i + 2) * (len / depth);
			}
			std::inplace_merge(
				data + i * (len / depth),
				data + (i + 1) * (len / depth),
				data + l
			);
		}
        }
}
```

Selon [la documentation](https://en.cppreference.com/w/cpp/algorithm/sort) la
fonction `std::sort()` a une complexitée en $O(N \log(N))$ en moyenne (ce qui
correspond en à un QuickSort). Notre nouvelle implémentation _s'il elle n'était
pas parallèle_ s'apparente à un MergeSort dont la dernière récursion est un
`std::sort()`.

Les performances mesurées sont les suivantes:

![](images/openmap_vs_naive.png
![](images/openmp_vs_naive_acc.png)

Nous constatons que pour moins de 5000 éléments dans la liste à trier l'intérêt
de cette implémentation à l'aide d'OpenMP est nulle et même négative.
De même à part pour le point à 10e5 éléments qui semble être une erreur de
mesure l'accélération est assez faible.

Bien que la machine ait 4 (hyper-)threads d'exécution, cette accélération
n'atteint jamais l'accélération maximale de x4. Ceci peut être très bien
expliqué par le fort couplage qu'il y entre la première boucle et la seconde :
en plus de parcourir la liste plusieurs fois (une fois pour les quick-sort et
une autre pour le merge), le processeur devra déplacer les parties du tableau
entre les différents coeurs. Ce qui même en utilisant les différents caches
(L1, L2 et L3) coute en temps.

### Seuil limite

TODO

### Influence de la structure des données


TODO

-----

Blague bonus sur le parallélisme pour les points d'humour :

C'est l'histoire d'un homme qui rentre dans un bar, le barman lui sert un
mojito puis l'hommme demande : "Pourrais-je avoir un mojito ?"

