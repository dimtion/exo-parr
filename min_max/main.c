
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <sys/time.h>

#include <string.h>
#include <immintrin.h>

static void minmax(float const* start, float const* stop, float* min, float* max);

#ifdef NAIVE
static void minmax(float const* start, float const* stop, float* min, float* max) {
  *min = *start;
  *max = *start;
  for (float const* c = start; c < stop; c++) {
    if (*c < *min) {
      *min = *c;
    }
    if (*c > *max) {
      *max = *c;
    }
  }
}
#endif

#ifdef AVX
#define VEC_LEN 8
static void minmax(float const* start, float const* stop, float* min, float* max) {
        float max_table[VEC_LEN];
        float min_table[VEC_LEN];
        size_t i;

        __m256 _vec;
        __m256 _max;
        __m256 _min;

        *max = *start;
        *min = *start;

        if (stop - start <= VEC_LEN) {
                for (const float*  c = start; stop; c++) {
                        if (*c > *max) *max = *c;
                        if (*c < *min) *min = *c;
                }
                return;
        }

        _max = _mm256_loadu_ps(start);
        _min = _mm256_loadu_ps(start);
        for (i = 0; i < (stop - start) / VEC_LEN; i++) {
                _vec = _mm256_loadu_ps(start + i * VEC_LEN);
                _max = _mm256_max_ps(_max, _vec);
                _min = _mm256_min_ps(_min, _vec);
        }
        // do the last element bit not multiple of 8
        // twice if necessary, does not matters.
        _vec = _mm256_loadu_ps(stop - VEC_LEN);
        _max = _mm256_max_ps(_max, _vec);
        _min = _mm256_min_ps(_min, _vec);

        _mm256_store_ps(max_table, _max);
        _mm256_store_ps(min_table, _min);

        // consume the max_table/min_table into the final result
        for (i = 0; i < VEC_LEN; i++) {
                if (max_table[i] > *max) *max = max_table[i];
                if (min_table[i] < *min) *min = min_table[i];
        }
}
#endif

int main(int argc, char**argv) {
    if(argc != 2) return 1;
    int n = atoi(argv[1]);
    float* data = malloc(n * sizeof(*data));
    for(int i = 0; i < n; ++i) {
        data[i] = (unsigned)i * (INT_MAX / 3 / n); // pourquoi ce cast?
    }

    struct timeval start, stop;
    float min, max;
    gettimeofday(&start, NULL);
    minmax(data, data + n, &min, &max);
    
    gettimeofday(&stop, NULL);
    printf("%f - %f\n", min, max);
    printf("%lf ms\n", (stop.tv_sec - start.tv_sec) * 1000. + (stop.tv_usec - start.tv_usec) / 1000.);
    return 0;
}

