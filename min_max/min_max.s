	.section	__TEXT,__text,regular,pure_instructions
	.build_version macos, 10, 14
	.section	__TEXT,__literal16,16byte_literals
	.p2align	4               ## -- Begin function main
LCPI0_0:
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	3                       ## 0x3
LCPI0_1:
	.long	4                       ## 0x4
	.long	4                       ## 0x4
	.long	4                       ## 0x4
	.long	4                       ## 0x4
LCPI0_2:
	.long	1258291200              ## 0x4b000000
	.long	1258291200              ## 0x4b000000
	.long	1258291200              ## 0x4b000000
	.long	1258291200              ## 0x4b000000
LCPI0_3:
	.long	1392508928              ## 0x53000000
	.long	1392508928              ## 0x53000000
	.long	1392508928              ## 0x53000000
	.long	1392508928              ## 0x53000000
LCPI0_4:
	.long	3539992704              ## float -5.49764202E+11
	.long	3539992704              ## float -5.49764202E+11
	.long	3539992704              ## float -5.49764202E+11
	.long	3539992704              ## float -5.49764202E+11
LCPI0_5:
	.long	8                       ## 0x8
	.long	8                       ## 0x8
	.long	8                       ## 0x8
	.long	8                       ## 0x8
LCPI0_6:
	.long	12                      ## 0xc
	.long	12                      ## 0xc
	.long	12                      ## 0xc
	.long	12                      ## 0xc
LCPI0_7:
	.long	16                      ## 0x10
	.long	16                      ## 0x10
	.long	16                      ## 0x10
	.long	16                      ## 0x10
	.section	__TEXT,__literal8,8byte_literals
	.p2align	3
LCPI0_8:
	.quad	4652007308841189376     ## double 1000
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.p2align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset %rbx, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	movl	$1, %r14d
	cmpl	$2, %edi
	jne	LBB0_39
## %bb.1:
	movq	8(%rsi), %rdi
	callq	_atoi
	movl	%eax, %r14d
	movslq	%r14d, %r15
	leaq	(,%r15,4), %rdi
	callq	_malloc
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jle	LBB0_13
## %bb.2:
	movl	$715827882, %eax        ## imm = 0x2AAAAAAA
	xorl	%edx, %edx
	divl	%r14d
	movl	%r14d, %ecx
	cmpl	$7, %r14d
	ja	LBB0_6
## %bb.3:
	xorl	%edx, %edx
	jmp	LBB0_4
LBB0_6:
	movl	%ecx, %edx
	andl	$-8, %edx
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm7        ## xmm7 = xmm0[0,0,0,0]
	leaq	-8(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$3, %rsi
	leal	1(%rsi), %r8d
	andl	$1, %r8d
	testq	%rdi, %rdi
	je	LBB0_7
## %bb.8:
	leaq	-1(%r8), %rdi
	subq	%rsi, %rdi
	movdqa	LCPI0_0(%rip), %xmm6    ## xmm6 = [0,1,2,3]
	xorl	%esi, %esi
	movdqa	LCPI0_1(%rip), %xmm8    ## xmm8 = [4,4,4,4]
	movdqa	LCPI0_2(%rip), %xmm3    ## xmm3 = [1258291200,1258291200,1258291200,1258291200]
	movdqa	LCPI0_3(%rip), %xmm4    ## xmm4 = [1392508928,1392508928,1392508928,1392508928]
	movaps	LCPI0_4(%rip), %xmm5    ## xmm5 = [-5.497642e+11,-5.497642e+11,-5.497642e+11,-5.497642e+11]
	movdqa	LCPI0_5(%rip), %xmm9    ## xmm9 = [8,8,8,8]
	movdqa	LCPI0_6(%rip), %xmm10   ## xmm10 = [12,12,12,12]
	movdqa	LCPI0_7(%rip), %xmm11   ## xmm11 = [16,16,16,16]
	.p2align	4, 0x90
LBB0_9:                                 ## =>This Inner Loop Header: Depth=1
	movdqa	%xmm6, %xmm0
	paddd	%xmm8, %xmm0
	movdqa	%xmm7, %xmm1
	pmulld	%xmm6, %xmm1
	pmulld	%xmm7, %xmm0
	movdqa	%xmm1, %xmm2
	pblendw	$170, %xmm3, %xmm2      ## xmm2 = xmm2[0],xmm3[1],xmm2[2],xmm3[3],xmm2[4],xmm3[5],xmm2[6],xmm3[7]
	psrld	$16, %xmm1
	pblendw	$170, %xmm4, %xmm1      ## xmm1 = xmm1[0],xmm4[1],xmm1[2],xmm4[3],xmm1[4],xmm4[5],xmm1[6],xmm4[7]
	addps	%xmm5, %xmm1
	addps	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	pblendw	$170, %xmm3, %xmm2      ## xmm2 = xmm2[0],xmm3[1],xmm2[2],xmm3[3],xmm2[4],xmm3[5],xmm2[6],xmm3[7]
	psrld	$16, %xmm0
	pblendw	$170, %xmm4, %xmm0      ## xmm0 = xmm0[0],xmm4[1],xmm0[2],xmm4[3],xmm0[4],xmm4[5],xmm0[6],xmm4[7]
	addps	%xmm5, %xmm0
	addps	%xmm2, %xmm0
	movups	%xmm1, (%rbx,%rsi,4)
	movups	%xmm0, 16(%rbx,%rsi,4)
	movdqa	%xmm6, %xmm0
	paddd	%xmm9, %xmm0
	movdqa	%xmm6, %xmm1
	paddd	%xmm10, %xmm1
	pmulld	%xmm7, %xmm0
	pmulld	%xmm7, %xmm1
	movdqa	%xmm0, %xmm2
	pblendw	$170, %xmm3, %xmm2      ## xmm2 = xmm2[0],xmm3[1],xmm2[2],xmm3[3],xmm2[4],xmm3[5],xmm2[6],xmm3[7]
	psrld	$16, %xmm0
	pblendw	$170, %xmm4, %xmm0      ## xmm0 = xmm0[0],xmm4[1],xmm0[2],xmm4[3],xmm0[4],xmm4[5],xmm0[6],xmm4[7]
	addps	%xmm5, %xmm0
	addps	%xmm2, %xmm0
	movdqa	%xmm1, %xmm2
	pblendw	$170, %xmm3, %xmm2      ## xmm2 = xmm2[0],xmm3[1],xmm2[2],xmm3[3],xmm2[4],xmm3[5],xmm2[6],xmm3[7]
	psrld	$16, %xmm1
	pblendw	$170, %xmm4, %xmm1      ## xmm1 = xmm1[0],xmm4[1],xmm1[2],xmm4[3],xmm1[4],xmm4[5],xmm1[6],xmm4[7]
	addps	%xmm5, %xmm1
	addps	%xmm2, %xmm1
	movups	%xmm0, 32(%rbx,%rsi,4)
	movups	%xmm1, 48(%rbx,%rsi,4)
	addq	$16, %rsi
	paddd	%xmm11, %xmm6
	addq	$2, %rdi
	jne	LBB0_9
## %bb.10:
	testq	%r8, %r8
	je	LBB0_12
LBB0_11:
	movdqa	%xmm7, %xmm0
	pmulld	%xmm6, %xmm0
	paddd	LCPI0_1(%rip), %xmm6
	pmulld	%xmm7, %xmm6
	movdqa	LCPI0_2(%rip), %xmm1    ## xmm1 = [1258291200,1258291200,1258291200,1258291200]
	movdqa	%xmm0, %xmm2
	pblendw	$170, %xmm1, %xmm2      ## xmm2 = xmm2[0],xmm1[1],xmm2[2],xmm1[3],xmm2[4],xmm1[5],xmm2[6],xmm1[7]
	psrld	$16, %xmm0
	movdqa	LCPI0_3(%rip), %xmm3    ## xmm3 = [1392508928,1392508928,1392508928,1392508928]
	pblendw	$170, %xmm3, %xmm0      ## xmm0 = xmm0[0],xmm3[1],xmm0[2],xmm3[3],xmm0[4],xmm3[5],xmm0[6],xmm3[7]
	movaps	LCPI0_4(%rip), %xmm4    ## xmm4 = [-5.497642e+11,-5.497642e+11,-5.497642e+11,-5.497642e+11]
	addps	%xmm4, %xmm0
	addps	%xmm2, %xmm0
	pblendw	$85, %xmm6, %xmm1       ## xmm1 = xmm6[0],xmm1[1],xmm6[2],xmm1[3],xmm6[4],xmm1[5],xmm6[6],xmm1[7]
	psrld	$16, %xmm6
	pblendw	$170, %xmm3, %xmm6      ## xmm6 = xmm6[0],xmm3[1],xmm6[2],xmm3[3],xmm6[4],xmm3[5],xmm6[6],xmm3[7]
	addps	%xmm4, %xmm6
	addps	%xmm1, %xmm6
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm6, 16(%rbx,%rsi,4)
LBB0_12:
	cmpq	%rcx, %rdx
	je	LBB0_13
LBB0_4:
	leaq	(%rbx,%rdx,4), %rsi
	subq	%rdx, %rcx
	.p2align	4, 0x90
LBB0_5:                                 ## =>This Inner Loop Header: Depth=1
	movl	%eax, %edi
	imull	%edx, %edi
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rdi, %xmm0
	movd	%xmm0, (%rsi)
	incq	%rdx
	addq	$4, %rsi
	decq	%rcx
	jne	LBB0_5
LBB0_13:
	xorl	%r14d, %r14d
	leaq	-80(%rbp), %rdi
	xorl	%esi, %esi
	callq	_gettimeofday
	leaq	(%rbx,%r15,4), %rdi
	movl	(%rbx), %eax
	movd	%eax, %xmm4
	cmpq	%rbx, %rdi
	jbe	LBB0_14
## %bb.15:
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %rdi
	jbe	LBB0_16
## %bb.17:
	movdqa	%xmm4, %xmm0
	maxss	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm4
	seta	%cl
	leaq	-5(,%r15,4), %r8
	testb	$4, %r8b
	jne	LBB0_18
## %bb.19:
	movdqa	%xmm4, %xmm1
	maxss	%xmm0, %xmm1
	movd	4(%rbx), %xmm2          ## xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm4
	movd	%xmm2, %edx
	movl	%eax, %esi
	cmoval	%edx, %esi
	movdqa	%xmm2, %xmm0
	cmpltss	%xmm4, %xmm0
	movaps	%xmm0, %xmm3
	andnps	%xmm1, %xmm3
	andps	%xmm4, %xmm0
	orps	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm2
	seta	%cl
	cmoval	%edx, %eax
	addq	$8, %rbx
	movq	%rbx, %rdx
	movdqa	%xmm2, %xmm4
	cmpq	$3, %r8
	je	LBB0_37
	.p2align	4, 0x90
LBB0_21:                                ## =>This Inner Loop Header: Depth=1
	movaps	%xmm4, %xmm1
	testb	$1, %cl
	jne	LBB0_23
## %bb.22:                              ##   in Loop: Header=BB0_21 Depth=1
	movaps	%xmm0, %xmm1
LBB0_23:                                ##   in Loop: Header=BB0_21 Depth=1
	movd	(%rdx), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movd	4(%rdx), %xmm4          ## xmm4 = mem[0],zero,zero,zero
	movd	%xmm0, %ebx
	movd	%esi, %xmm2
	ucomiss	%xmm0, %xmm2
	movl	%ebx, %ecx
	jbe	LBB0_24
## %bb.25:                              ##   in Loop: Header=BB0_21 Depth=1
	ja	LBB0_26
LBB0_27:                                ##   in Loop: Header=BB0_21 Depth=1
	ucomiss	%xmm1, %xmm0
	ja	LBB0_29
LBB0_28:                                ##   in Loop: Header=BB0_21 Depth=1
	movl	%eax, %ebx
LBB0_29:                                ##   in Loop: Header=BB0_21 Depth=1
	movd	%xmm4, %eax
	movd	%ecx, %xmm2
	ucomiss	%xmm4, %xmm2
	movl	%eax, %esi
	ja	LBB0_31
## %bb.30:                              ##   in Loop: Header=BB0_21 Depth=1
	movl	%ecx, %esi
LBB0_31:                                ##   in Loop: Header=BB0_21 Depth=1
	ja	LBB0_32
## %bb.33:                              ##   in Loop: Header=BB0_21 Depth=1
	maxss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm4
	seta	%cl
	jbe	LBB0_35
	jmp	LBB0_36
	.p2align	4, 0x90
LBB0_24:                                ##   in Loop: Header=BB0_21 Depth=1
	movl	%esi, %ecx
	jbe	LBB0_27
LBB0_26:                                ##   in Loop: Header=BB0_21 Depth=1
	movd	%eax, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	LBB0_28
	jmp	LBB0_29
	.p2align	4, 0x90
LBB0_32:                                ##   in Loop: Header=BB0_21 Depth=1
	movd	%ebx, %xmm0
	ucomiss	%xmm0, %xmm4
	seta	%cl
	ja	LBB0_36
LBB0_35:                                ##   in Loop: Header=BB0_21 Depth=1
	movl	%ebx, %eax
LBB0_36:                                ##   in Loop: Header=BB0_21 Depth=1
	addq	$8, %rdx
	cmpq	%rdi, %rdx
	jb	LBB0_21
	jmp	LBB0_37
LBB0_14:
	movd	%xmm4, -28(%rbp)        ## 4-byte Folded Spill
	movdqa	%xmm4, -48(%rbp)        ## 16-byte Spill
	jmp	LBB0_38
LBB0_16:
	movl	%eax, %esi
	jmp	LBB0_37
LBB0_18:
	movl	%eax, %esi
	cmpq	$3, %r8
	jne	LBB0_21
LBB0_37:
	movd	%esi, %xmm0
	movd	%xmm0, -28(%rbp)        ## 4-byte Folded Spill
	movd	%eax, %xmm0
	movdqa	%xmm0, -48(%rbp)        ## 16-byte Spill
LBB0_38:
	leaq	-64(%rbp), %rdi
	xorl	%esi, %esi
	callq	_gettimeofday
	movss	-28(%rbp), %xmm0        ## 4-byte Reload
                                        ## xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movaps	-48(%rbp), %xmm1        ## 16-byte Reload
	cvtss2sd	%xmm1, %xmm1
	leaq	L_.str(%rip), %rdi
	movb	$2, %al
	callq	_printf
	movq	-64(%rbp), %rax
	subq	-80(%rbp), %rax
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	movsd	LCPI0_8(%rip), %xmm2    ## xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movl	-56(%rbp), %eax
	subl	-72(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	divsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	leaq	L_.str.1(%rip), %rdi
	movb	$1, %al
	callq	_printf
LBB0_39:
	movl	%r14d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_7:
	movdqa	LCPI0_0(%rip), %xmm6    ## xmm6 = [0,1,2,3]
	xorl	%esi, %esi
	testq	%r8, %r8
	jne	LBB0_11
	jmp	LBB0_12
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"%f - %f\n"

L_.str.1:                               ## @.str.1
	.asciz	"%lf ms\n"


.subsections_via_symbols
