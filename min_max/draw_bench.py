#!/usr/bin/env python3

"""
From stdin read lines like `N: T ms` and plot a graph from it
Separated by `--- bench title`
"""

import sys
from matplotlib import pyplot as plt

n_iters = []
exec_dur = []
legends = []

exp = -1

for line in sys.stdin:
    print(line.strip())
    if "---" in line:
        n_iters.append([])
        exec_dur.append([])
        exp += 1
        legends.append("".join(line.split()[1:]))
        continue

    (n, t, _) = line.strip().split()
    n = n.strip(":")
    n_iters[exp].append(int(n))
    exec_dur[exp].append(float(t))

for n, e in zip(n_iters, exec_dur):
    plt.loglog(n, e)
plt.xlabel("# iterations")
plt.ylabel("Duration (ms)")
plt.legend(legends)
plt.title(sys.argv[1])
plt.grid()
plt.show()

# acceleration
if len(exec_dur) <= 1:
    exit()
acc = [
    [d1 / d2 for d1, d2 in zip(exec_dur[0], duration2)]
    for duration2 in exec_dur[1:]
]
for n, e in zip(n_iters, acc):
    plt.plot(n, e)
plt.xlabel("# iterations")
plt.ylabel("Acceleration")
plt.semilogx()
plt.legend(legends[1:])
plt.title(sys.argv[1] + " acceleration")
plt.grid()
plt.show()
