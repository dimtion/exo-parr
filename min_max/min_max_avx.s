	.section	__TEXT,__text,regular,pure_instructions
	.build_version macos, 10, 14
	.section	__TEXT,__const
	.p2align	5               ## -- Begin function main
LCPI0_0:
	.long	0                       ## 0x0
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	3                       ## 0x3
	.long	4                       ## 0x4
	.long	5                       ## 0x5
	.long	6                       ## 0x6
	.long	7                       ## 0x7
LCPI0_1:
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
	.long	1199570944              ## float 65536
LCPI0_2:
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.long	65535                   ## 0xffff
	.section	__TEXT,__literal16,16byte_literals
	.p2align	4
LCPI0_3:
	.long	8                       ## 0x8
	.long	8                       ## 0x8
	.long	8                       ## 0x8
	.long	8                       ## 0x8
LCPI0_4:
	.long	16                      ## 0x10
	.long	16                      ## 0x10
	.long	16                      ## 0x10
	.long	16                      ## 0x10
LCPI0_5:
	.long	24                      ## 0x18
	.long	24                      ## 0x18
	.long	24                      ## 0x18
	.long	24                      ## 0x18
LCPI0_6:
	.long	32                      ## 0x20
	.long	32                      ## 0x20
	.long	32                      ## 0x20
	.long	32                      ## 0x20
	.section	__TEXT,__literal8,8byte_literals
	.p2align	3
LCPI0_7:
	.quad	4652007308841189376     ## double 1000
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.p2align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	movl	$1, %ebx
	cmpl	$2, %edi
	jne	LBB0_64
## %bb.1:
	movq	8(%rsi), %rdi
	callq	_atoi
	movl	%eax, %r15d
	movslq	%r15d, %r12
	leaq	(,%r12,4), %r14
	movq	%r14, %rdi
	callq	_malloc
	movq	%rax, %rbx
	testl	%r12d, %r12d
	jle	LBB0_14
## %bb.2:
	movl	$715827882, %eax        ## imm = 0x2AAAAAAA
	xorl	%edx, %edx
	divl	%r15d
	movl	%r15d, %ecx
	cmpl	$7, %r15d
	ja	LBB0_6
## %bb.3:
	xorl	%edx, %edx
	jmp	LBB0_4
LBB0_6:
	movl	%ecx, %edx
	andl	$-8, %edx
	vmovd	%eax, %xmm0
	vpshufd	$0, %xmm0, %xmm0        ## xmm0 = xmm0[0,0,0,0]
	vinsertf128	$1, %xmm0, %ymm0, %ymm0
	leaq	-8(%rdx), %rsi
	movq	%rsi, %rdi
	shrq	$3, %rdi
	leal	1(%rdi), %r8d
	andl	$3, %r8d
	cmpq	$24, %rsi
	jae	LBB0_8
## %bb.7:
	vmovdqa	LCPI0_0(%rip), %ymm1    ## ymm1 = [0,1,2,3,4,5,6,7]
	xorl	%edi, %edi
	testq	%r8, %r8
	jne	LBB0_11
	jmp	LBB0_13
LBB0_8:
	leaq	-1(%r8), %rsi
	subq	%rdi, %rsi
	vmovdqa	LCPI0_0(%rip), %ymm1    ## ymm1 = [0,1,2,3,4,5,6,7]
	xorl	%edi, %edi
	vextractf128	$1, %ymm0, %xmm2
	vmovaps	LCPI0_1(%rip), %ymm3    ## ymm3 = [6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04]
	vmovaps	LCPI0_2(%rip), %ymm4    ## ymm4 = [65535,65535,65535,65535,65535,65535,65535,65535]
	vmovdqa	LCPI0_3(%rip), %xmm8    ## xmm8 = [8,8,8,8]
	vmovdqa	LCPI0_4(%rip), %xmm9    ## xmm9 = [16,16,16,16]
	vmovdqa	LCPI0_5(%rip), %xmm10   ## xmm10 = [24,24,24,24]
	vmovdqa	LCPI0_6(%rip), %xmm13   ## xmm13 = [32,32,32,32]
	.p2align	4, 0x90
LBB0_9:                                 ## =>This Inner Loop Header: Depth=1
	vextractf128	$1, %ymm1, %xmm6
	vpmulld	%xmm6, %xmm2, %xmm7
	vpmulld	%xmm1, %xmm0, %xmm11
	vinsertf128	$1, %xmm7, %ymm11, %ymm12
	vpsrld	$16, %xmm11, %xmm11
	vpsrld	$16, %xmm7, %xmm7
	vinsertf128	$1, %xmm7, %ymm11, %ymm7
	vcvtdq2ps	%ymm7, %ymm7
	vmulps	%ymm3, %ymm7, %ymm7
	vandps	%ymm4, %ymm12, %ymm11
	vcvtdq2ps	%ymm11, %ymm11
	vaddps	%ymm11, %ymm7, %ymm7
	vmovups	%ymm7, (%rbx,%rdi,4)
	vpaddd	%xmm8, %xmm1, %xmm7
	vpaddd	%xmm8, %xmm6, %xmm5
	vpmulld	%xmm5, %xmm2, %xmm5
	vpmulld	%xmm7, %xmm0, %xmm7
	vinsertf128	$1, %xmm5, %ymm7, %ymm11
	vpsrld	$16, %xmm7, %xmm7
	vpsrld	$16, %xmm5, %xmm5
	vinsertf128	$1, %xmm5, %ymm7, %ymm5
	vcvtdq2ps	%ymm5, %ymm5
	vmulps	%ymm3, %ymm5, %ymm5
	vandps	%ymm4, %ymm11, %ymm7
	vcvtdq2ps	%ymm7, %ymm7
	vaddps	%ymm7, %ymm5, %ymm5
	vmovups	%ymm5, 32(%rbx,%rdi,4)
	vpaddd	%xmm9, %xmm1, %xmm5
	vpaddd	%xmm9, %xmm6, %xmm7
	vpmulld	%xmm7, %xmm2, %xmm7
	vpmulld	%xmm5, %xmm0, %xmm5
	vinsertf128	$1, %xmm7, %ymm5, %ymm11
	vpsrld	$16, %xmm5, %xmm5
	vpsrld	$16, %xmm7, %xmm7
	vinsertf128	$1, %xmm7, %ymm5, %ymm5
	vcvtdq2ps	%ymm5, %ymm5
	vmulps	%ymm3, %ymm5, %ymm5
	vandps	%ymm4, %ymm11, %ymm7
	vcvtdq2ps	%ymm7, %ymm7
	vaddps	%ymm7, %ymm5, %ymm5
	vmovups	%ymm5, 64(%rbx,%rdi,4)
	vpaddd	%xmm10, %xmm1, %xmm5
	vpaddd	%xmm10, %xmm6, %xmm7
	vpmulld	%xmm7, %xmm2, %xmm7
	vpmulld	%xmm5, %xmm0, %xmm5
	vinsertf128	$1, %xmm7, %ymm5, %ymm11
	vpsrld	$16, %xmm5, %xmm5
	vpsrld	$16, %xmm7, %xmm7
	vinsertf128	$1, %xmm7, %ymm5, %ymm5
	vcvtdq2ps	%ymm5, %ymm5
	vmulps	%ymm3, %ymm5, %ymm5
	vandps	%ymm4, %ymm11, %ymm7
	vcvtdq2ps	%ymm7, %ymm7
	vaddps	%ymm7, %ymm5, %ymm5
	vmovups	%ymm5, 96(%rbx,%rdi,4)
	addq	$32, %rdi
	vpaddd	%xmm13, %xmm6, %xmm5
	vpaddd	%xmm13, %xmm1, %xmm1
	vinsertf128	$1, %xmm5, %ymm1, %ymm1
	addq	$4, %rsi
	jne	LBB0_9
## %bb.10:
	testq	%r8, %r8
	je	LBB0_13
LBB0_11:
	leaq	(%rbx,%rdi,4), %rsi
	negq	%r8
	vextractf128	$1, %ymm0, %xmm2
	vmovaps	LCPI0_2(%rip), %ymm3    ## ymm3 = [65535,65535,65535,65535,65535,65535,65535,65535]
	vmovaps	LCPI0_1(%rip), %ymm4    ## ymm4 = [6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04,6.553600e+04]
	vmovdqa	LCPI0_3(%rip), %xmm5    ## xmm5 = [8,8,8,8]
	.p2align	4, 0x90
LBB0_12:                                ## =>This Inner Loop Header: Depth=1
	vextractf128	$1, %ymm1, %xmm6
	vpmulld	%xmm6, %xmm2, %xmm7
	vpmulld	%xmm1, %xmm0, %xmm8
	vinsertf128	$1, %xmm7, %ymm8, %ymm9
	vandps	%ymm3, %ymm9, %ymm9
	vcvtdq2ps	%ymm9, %ymm9
	vpsrld	$16, %xmm8, %xmm8
	vpsrld	$16, %xmm7, %xmm7
	vinsertf128	$1, %xmm7, %ymm8, %ymm7
	vcvtdq2ps	%ymm7, %ymm7
	vmulps	%ymm4, %ymm7, %ymm7
	vaddps	%ymm9, %ymm7, %ymm7
	vmovups	%ymm7, (%rsi)
	vpaddd	%xmm5, %xmm6, %xmm6
	vpaddd	%xmm5, %xmm1, %xmm1
	vinsertf128	$1, %xmm6, %ymm1, %ymm1
	addq	$32, %rsi
	incq	%r8
	jne	LBB0_12
LBB0_13:
	cmpq	%rcx, %rdx
	je	LBB0_14
LBB0_4:
	leaq	(%rbx,%rdx,4), %rsi
	subq	%rdx, %rcx
	.p2align	4, 0x90
LBB0_5:                                 ## =>This Inner Loop Header: Depth=1
	movl	%eax, %edi
	imull	%edx, %edi
	vcvtsi2ssq	%rdi, %xmm14, %xmm0
	vmovd	%xmm0, (%rsi)
	incq	%rdx
	addq	$4, %rsi
	decq	%rcx
	jne	LBB0_5
LBB0_14:
	leaq	-72(%rbp), %rdi
	xorl	%esi, %esi
	vzeroupper
	callq	_gettimeofday
	leaq	(%rbx,%r12,4), %rax
	movl	(%rbx), %r15d
	vmovd	%r15d, %xmm7
	cmpq	$35, %r14
	jg	LBB0_17
## %bb.15:
	testq	%rax, %rax
	je	LBB0_63
	.p2align	4, 0x90
LBB0_16:                                ## =>This Inner Loop Header: Depth=1
	jmp	LBB0_16
LBB0_17:
	movq	%r14, %rcx
	shrq	$2, %rcx
	vmovups	(%rbx), %ymm0
	addq	$7, %rcx
	cmpq	$15, %rcx
	jb	LBB0_18
## %bb.19:
	shrq	$5, %r14
	xorl	%ecx, %ecx
	vmovaps	%ymm0, %ymm1
	.p2align	4, 0x90
LBB0_20:                                ## =>This Inner Loop Header: Depth=1
	vmovups	(%rbx), %ymm2
	vmaxps	%ymm2, %ymm1, %ymm1
	vminps	%ymm2, %ymm0, %ymm0
	incq	%rcx
	addq	$32, %rbx
	cmpq	%r14, %rcx
	jb	LBB0_20
	jmp	LBB0_21
LBB0_18:
	vmovaps	%ymm0, %ymm1
LBB0_21:
	vmovups	-32(%rax), %ymm3
	vmaxps	%ymm3, %ymm1, %ymm1
	vmovd	%xmm1, %eax
	vmovd	%eax, %xmm2
	vucomiss	%xmm7, %xmm2
	vminps	%ymm3, %ymm0, %ymm0
	cmovbel	%r15d, %eax
	vmovd	%xmm0, %ecx
	vmovd	%ecx, %xmm3
	vucomiss	%xmm3, %xmm7
	vmovshdup	%xmm1, %xmm4    ## xmm4 = xmm1[1,1,3,3]
	cmoval	%ecx, %r15d
	ja	LBB0_22
## %bb.23:
	vmaxss	%xmm7, %xmm2, %xmm5
	jmp	LBB0_24
LBB0_22:
	vmovd	%eax, %xmm5
LBB0_24:
	vmovshdup	%xmm0, %xmm2    ## xmm2 = xmm0[1,1,3,3]
	vucomiss	%xmm5, %xmm4
	vmovd	%xmm4, %ecx
	cmoval	%ecx, %eax
	ja	LBB0_25
## %bb.26:
	vminss	%xmm7, %xmm3, %xmm6
	jmp	LBB0_27
LBB0_25:
	vmovd	%r15d, %xmm6
LBB0_27:
	vpermilpd	$1, %xmm1, %xmm3 ## xmm3 = xmm1[1,0]
	vucomiss	%xmm2, %xmm6
	vmovd	%xmm2, %ecx
	cmoval	%ecx, %r15d
	ja	LBB0_28
## %bb.29:
	vmaxss	%xmm5, %xmm4, %xmm5
	jmp	LBB0_30
LBB0_28:
	vmovd	%eax, %xmm5
LBB0_30:
	vpermilpd	$1, %xmm0, %xmm4 ## xmm4 = xmm0[1,0]
	vucomiss	%xmm5, %xmm3
	vmovd	%xmm3, %ecx
	cmoval	%ecx, %eax
	ja	LBB0_31
## %bb.32:
	vminss	%xmm6, %xmm2, %xmm6
	jmp	LBB0_33
LBB0_31:
	vmovd	%r15d, %xmm6
LBB0_33:
	vpshufd	$231, %xmm1, %xmm2      ## xmm2 = xmm1[3,1,2,3]
	vucomiss	%xmm4, %xmm6
	vmovd	%xmm4, %ecx
	cmoval	%ecx, %r15d
	ja	LBB0_34
## %bb.35:
	vmaxss	%xmm5, %xmm3, %xmm5
	jmp	LBB0_36
LBB0_34:
	vmovd	%eax, %xmm5
LBB0_36:
	vpshufd	$231, %xmm0, %xmm3      ## xmm3 = xmm0[3,1,2,3]
	vucomiss	%xmm5, %xmm2
	vmovd	%xmm2, %ecx
	cmoval	%ecx, %eax
	ja	LBB0_37
## %bb.38:
	vminss	%xmm6, %xmm4, %xmm4
	jmp	LBB0_39
LBB0_37:
	vmovd	%r15d, %xmm4
LBB0_39:
	vextractf128	$1, %ymm1, %xmm1
	vucomiss	%xmm3, %xmm4
	vmovd	%xmm3, %ecx
	cmoval	%ecx, %r15d
	ja	LBB0_40
## %bb.41:
	vmaxss	%xmm5, %xmm2, %xmm2
	jmp	LBB0_42
LBB0_40:
	vmovd	%eax, %xmm2
LBB0_42:
	vextractf128	$1, %ymm0, %xmm0
	vucomiss	%xmm2, %xmm1
	vmovd	%xmm1, %ecx
	cmoval	%ecx, %eax
	ja	LBB0_43
## %bb.44:
	vminss	%xmm4, %xmm3, %xmm4
	jmp	LBB0_45
LBB0_43:
	vmovd	%r15d, %xmm4
LBB0_45:
	vmovshdup	%xmm1, %xmm3    ## xmm3 = xmm1[1,1,3,3]
	vucomiss	%xmm0, %xmm4
	vmovd	%xmm0, %ecx
	cmoval	%ecx, %r15d
	ja	LBB0_46
## %bb.47:
	vmaxss	%xmm2, %xmm1, %xmm6
	jmp	LBB0_48
LBB0_46:
	vmovd	%eax, %xmm6
LBB0_48:
	vmovshdup	%xmm0, %xmm2    ## xmm2 = xmm0[1,1,3,3]
	vucomiss	%xmm6, %xmm3
	vmovd	%xmm3, %ecx
	cmoval	%ecx, %eax
	ja	LBB0_49
## %bb.50:
	vminss	%xmm4, %xmm0, %xmm8
	jmp	LBB0_51
LBB0_49:
	vmovd	%r15d, %xmm8
LBB0_51:
	vpermilpd	$1, %xmm1, %xmm5 ## xmm5 = xmm1[1,0]
	vucomiss	%xmm2, %xmm8
	vmovd	%xmm2, %ecx
	cmoval	%ecx, %r15d
	ja	LBB0_52
## %bb.53:
	vmaxss	%xmm6, %xmm3, %xmm7
	jmp	LBB0_54
LBB0_52:
	vmovd	%eax, %xmm7
LBB0_54:
	vpermilpd	$1, %xmm0, %xmm3 ## xmm3 = xmm0[1,0]
	vucomiss	%xmm7, %xmm5
	vmovd	%xmm5, %ecx
	vmovd	%r15d, %xmm4
	cmoval	%ecx, %eax
	vmovdqa	%xmm4, %xmm6
	ja	LBB0_56
## %bb.55:
	vminss	%xmm8, %xmm2, %xmm6
LBB0_56:
	vpshufd	$231, %xmm1, %xmm1      ## xmm1 = xmm1[3,1,2,3]
	vmaxss	%xmm7, %xmm5, %xmm2
	vucomiss	%xmm3, %xmm6
	vmovd	%eax, %xmm5
	vcmpltss	%xmm6, %xmm3, %xmm7
	vblendvps	%xmm7, %xmm5, %xmm2, %xmm2
	vmovaps	%xmm3, %xmm7
	ja	LBB0_58
## %bb.57:
	vmovdqa	%xmm4, %xmm7
LBB0_58:
	vpermilps	$231, %xmm0, %xmm0 ## xmm0 = xmm0[3,1,2,3]
	vucomiss	%xmm2, %xmm1
	vmovd	%xmm1, %ecx
	cmoval	%ecx, %eax
	vmovaps	%xmm7, %xmm1
	ja	LBB0_60
## %bb.59:
	vminss	%xmm6, %xmm3, %xmm1
LBB0_60:
	vucomiss	%xmm0, %xmm1
	jbe	LBB0_62
## %bb.61:
	vmovaps	%xmm0, %xmm7
LBB0_62:
	movl	%eax, %r15d
LBB0_63:
	vmovss	%xmm7, -36(%rbp)        ## 4-byte Spill
	xorl	%ebx, %ebx
	leaq	-56(%rbp), %rdi
	xorl	%esi, %esi
	vzeroupper
	callq	_gettimeofday
	vmovss	-36(%rbp), %xmm0        ## 4-byte Reload
                                        ## xmm0 = mem[0],zero,zero,zero
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vmovd	%r15d, %xmm1
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	leaq	L_.str(%rip), %rdi
	movb	$2, %al
	callq	_printf
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	vcvtsi2sdq	%rax, %xmm14, %xmm0
	vmovsd	LCPI0_7(%rip), %xmm1    ## xmm1 = mem[0],zero
	vmulsd	%xmm1, %xmm0, %xmm0
	movl	-48(%rbp), %eax
	subl	-64(%rbp), %eax
	vcvtsi2sdl	%eax, %xmm14, %xmm2
	vdivsd	%xmm1, %xmm2, %xmm1
	vaddsd	%xmm1, %xmm0, %xmm0
	leaq	L_.str.1(%rip), %rdi
	movb	$1, %al
	callq	_printf
LBB0_64:
	movl	%ebx, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"%f - %f\n"

L_.str.1:                               ## @.str.1
	.asciz	"%lf ms\n"


.subsections_via_symbols
