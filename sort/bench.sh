#!/usr/bin/env bash

echo "--- $2"
for i in 1000 5000 10000 50000 100000 500000 1000000 5000000 10000000 50000000 100000000 ; do 
        echo "$i: $(./$1 $i | grep ms)";
done
