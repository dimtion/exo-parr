#include <algorithm>
#include <iostream>
#include <limits>
#include <sys/time.h>

#define SORT_E double
#define PARR 4

void sort(SORT_E *data, int len) {
	int i;
        #pragma omp parallel for
	for(i=0; i <= PARR; i++) {
		int l;
		if (i == PARR) {
			l = len;
		} else {
			l = (i + 1) * (len / PARR);
		}
		std::sort(
			data + i * (len / PARR),
			data + l
		);
	}
	int depth = PARR;
	for (depth=PARR; depth > 1; depth>>=1) {
		for (i=0; i < depth; i+=2) {
			int l;
			if (i + 2 == depth) {
				l = len;
			} else {
				l = (i + 2) * (len / depth);
			}
			std::inplace_merge(
				data + i * (len / depth),
				data + (i + 1) * (len / depth),
				data + l
			);
		}
        }
}

int main(int argc, char**argv) {
	if (argc != 2) return 1;
	int n = std::stoi(argv[1]);
	SORT_E *data = new SORT_E [n];
	for (int i = 0; i < n; ++i)
		data[i] = (SORT_E)(n - i) * (std::numeric_limits<int>::max() / 3);

	struct timeval start, stop;
	gettimeofday(&start, NULL);
	sort(data, n);
	gettimeofday(&stop, NULL);
	volatile __attribute__((unused)) SORT_E anchor = data[n/2]; // why that?
	std::cout << ((stop.tv_sec - start.tv_sec) * 1000. + (stop.tv_usec - start.tv_usec) / 1000.) << " ms\n";
	delete [] data;
	return 0;
}



