#include <algorithm>
#include <iostream>
#include <limits>
#include <sys/time.h>

void sort(double *data, int len) {
	std::sort(data, data + len);
}
int main(int argc, char**argv) {
	if (argc != 2) return 1;
	int n = std::stoi(argv[1]);
	double *data = new double [n];
	for (int i = 0; i < n; ++i)
		data[i] = (double)i * (std::numeric_limits<int>::max() / 3);

	struct timeval start, stop;
	gettimeofday(&start, NULL);
	sort(data, n);
	gettimeofday(&stop, NULL);
	volatile __attribute__((unused)) double anchor = data[n/2]; // why that?
	std::cout << ((stop.tv_sec - start.tv_sec) * 1000. + (stop.tv_usec - start.tv_usec) / 1000.) << " ms\n";
	delete [] data;
	return 0;
}



